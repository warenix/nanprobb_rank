from babies import update_babies_count
import dropbox

access_token = '<RELACE_YOUR_DROPBOX_ACCESS_TOKEN>'
client = dropbox.client.DropboxClient(access_token)
print 'linked account: ', client.account_info()

baby_file = 'baby.json'
json = update_babies_count(max_thread_id=24)
f = open(baby_file, 'w+')
f.write(json)
f.close()

f = open(baby_file, 'rb')
response = client.put_file('/Public/%s' % baby_file, f, overwrite=True)
print "uploaded:", response
