# demo script to get all babies and rank them by votes
# Include the Dropbox SDK
# https://promo.nestle.com.hk/nanprobb/
# date: 6 Jun 2014
# author: warenix
# usage: python babies.py > ~/Desktop/baby.json
# then upload baby.json to dropbox public foler
import urllib, urllib2
import json
import threading, time

def _compare_keys(x, y):
    try:
        x = int(x)
    except ValueError:
        xint = False
    else:
        xint = True
    try:
        y = int(y)
    except ValueError:
        if xint:
            return -1
        return cmp(x.lower(), y.lower())
        # or cmp(x, y) if you want case sensitivity.
    else:
        if xint:
            return cmp(x, y)
        return 1

def fetch_new_babies(page_index=1, retry_count=10):
    d={
        "PageIndex":page_index,
        "BabyName":"",
        "Email":"",
        "OrderType":1,
    }
    params = urllib.urlencode(d)
    req = urllib2.Request("https://promo.nestle.com.hk/nanprobb/happytummy/ajax/babies.aspx", params)
    req.add_header('Referer', 'https://promo.nestle.com.hk/nanprobb/happytummy/babies.aspx')
    req.add_header('Content-type', "application/x-www-form-urlencoded")
    req.add_header('Content-length', len(params))
    req.add_header('Accept', "text/plain")
    req.add_header('User-Agent', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:29.0) Gecko/20100101 Firefox/29.0')

    backoff_time = 1
    while retry_count > 0:
        retry_count -= 1
        try:
            f = urllib2.urlopen(req)
            s = f.read()
            j = json.loads(s)
            babies = j['Babies']
            return babies
        except urllib2.URLError, e:
            print 'cannot fetch babies for page[%d] retry_count[%d]' % (page_index, retry_count)
            time.sleep(backoff_time)
            backoff_time += backoff_time
            pass

all_babies = []
babyLock = threading.Lock()
threadLock = threading.Lock()
threads = []
next_page_index = 1
noMore = False

class FetchBabiesThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):

        while True:
            current_page_index = self.get_next_page_index()
            if current_page_index == -1:# or current_page_index > 10:
                break
            time.sleep(1)

            #print current_page_index
            babies = fetch_new_babies(current_page_index)
            if len(babies) == 0:
                #print 'last page is %d' % current_page_index
                noMore = True
                break
            else:
                babyLock.acquire()
                global all_babies
                all_babies = all_babies + babies
                babyLock.release()
                #print len(all_babies)

            return


    def get_next_page_index(self):
        if noMore:
            return -1
        threadLock.acquire()
        global next_page_index
        current_page_index = next_page_index
        next_page_index += 1
        threadLock.release()
        return current_page_index


def update_babies_count(max_thread_id=16):
    thread_id = 1

    while thread_id <= max_thread_id:
        t = FetchBabiesThread()
        threads.append(t)
        t.start()
        thread_id += 1

    for t in threads:
        t.join()

    outputJson = []
    #print 'sorting'

    from operator import itemgetter

    sorted_babies=sorted(all_babies, key=itemgetter('VoteCount'), cmp=_compare_keys, reverse=True)
    rank = 0
    last_vote = -1
    vote = 0
    for b  in sorted_babies:
        if b['VoteCount'] != '0':
            vote = int(b['VoteCount'])
            if last_vote != vote:
                rank+=1
            last_vote = vote
            #print b['Id'], b['EmailAddress'], b['VoteCount']
            outputJson.append({
            'Id':b['Id'],
            'VoteCount':b['VoteCount'],
            'PhotoWithFramePath':b['PhotoWithFramePath'],
            'Rank':rank,
            })
    #print sorted_babies[-1]
    return json.dumps(outputJson)

if __name__=='__main__':
    json = update_babies_count(1)
    print json
