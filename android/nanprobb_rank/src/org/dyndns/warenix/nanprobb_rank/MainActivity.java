package org.dyndns.warenix.nanprobb_rank;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(new DataHandlerFragment(), "data").commit();
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new RankListFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		private static final String TAG = "PlaceholderFragment";

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.baby_item, container,
					false);
			return rootView;
		}
	}

	public static class TextUtil {
		public static void applyCustomFont(Context context, TextView textView) {
			Typeface face;
			face = Typeface.createFromAsset(context.getAssets(),
					"fonts/VERSIONTYPe-Regular.ttf");
			textView.setTypeface(face);
		}
	}

	public void toggleActionBar() {
		ActionBar actionBar = getSupportActionBar();

		if (actionBar != null) {
			toggleActionBar(!actionBar.isShowing());
		}
	}

	public void toggleActionBar(boolean show) {
		ActionBar actionBar = getSupportActionBar();

		if (actionBar != null) {
			if (show) {
				actionBar.show();
			} else {
				actionBar.hide();
			}
		}
	}
}
