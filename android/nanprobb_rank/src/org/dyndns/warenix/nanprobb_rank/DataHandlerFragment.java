package org.dyndns.warenix.nanprobb_rank;

import java.io.IOException;
import java.util.HashMap;

import org.dyndns.warenix.nanprobb_rank.eventbus.LoadDataRequest;
import org.dyndns.warenix.nanprobb_rank.eventbus.LoadDataResult;
import org.dyndns.warenix.nanprobb_rank.eventbus.SearchBabyRequest;
import org.dyndns.warenix.nanprobb_rank.eventbus.SearchBabyResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.database.MatrixCursor;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import de.greenrobot.event.EventBus;

public class DataHandlerFragment extends Fragment {
	private static final String TAG = "DataHandlerFragment";
	private static final String dataUrl = "https://dl.dropboxusercontent.com/u/3719247/baby.json";
	private static JSONArray jsonArray;

	/**
	 * key: baby Id, value: index in the cursor
	 */
	private static final HashMap<String, Integer> sBabyIndexMap = new HashMap<String, Integer>();

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		EventBus.getDefault().unregister(this);
	}

	public void onEventBackgroundThread(LoadDataRequest request) {

		try {
			if (jsonArray == null) {
				String jsonString = getData();
				jsonArray = new JSONArray(jsonString);
				Log.d(TAG, String.format("babies size[%d]", jsonArray.length()));
			}
			if (jsonArray != null) {
				// convert to cursor
				MatrixCursor cursor = new MatrixCursor(new String[] { "_id",
						"Id", "PhotoWithFramePath", "VoteCount", "Rank" });
				int l = jsonArray.length();
				JSONObject json = null;
				for (int i = 0; i < l; ++i) {
					json = jsonArray.getJSONObject(i);
					cursor.addRow(new Object[] { i, json.getString("Id"),
							json.getString("PhotoWithFramePath"),
							json.getString("VoteCount"),
							json.getString("Rank"), });

					sBabyIndexMap.put(json.getString("Id"), i);
				}

				EventBus.getDefault().post(new LoadDataResult(cursor));
			} else {
				Log.w(TAG, String.format("no data is retrieved"));
			}

		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private String getData() throws IOException {
		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder().url(dataUrl).build();

		Response response = client.newCall(request).execute();
		return response.body().string();
	}

	public void onEventBackgroundThread(SearchBabyRequest request) {
		Integer index = sBabyIndexMap.get(request.Id);
		if (index != null) {
			EventBus.getDefault().post(new SearchBabyResult(index));
		} else {
			EventBus.getDefault().post(new SearchBabyResult(-1));
		}
	}
}
