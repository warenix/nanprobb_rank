package org.dyndns.warenix.nanprobb_rank;

import org.dyndns.warenix.nanprobb_rank.MainActivity.TextUtil;
import org.dyndns.warenix.nanprobb_rank.eventbus.LoadDataRequest;
import org.dyndns.warenix.nanprobb_rank.eventbus.LoadDataResult;
import org.dyndns.warenix.nanprobb_rank.eventbus.SearchBabyRequest;
import org.dyndns.warenix.nanprobb_rank.eventbus.SearchBabyResult;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import de.greenrobot.event.EventBus;

public class RankListFragment extends ListFragment {

	private BabyCursorAdapter adapter;
	private boolean mLoaded;

	public static final String KEY_LAST_BABY_ID = "org.dyndns.warenix.nanprobb_rank.RankListFragment.KEY_LAST_BABY_ID";

	public RankListFragment() {
		setHasOptionsMenu(true);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getListView().setFastScrollEnabled(true);

		View headerView = getActivity().getLayoutInflater().inflate(
				R.layout.emtpy_listview_header, null);
		getListView().addHeaderView(headerView);

		getListView().setOnScrollListener(new OnScrollListener() {

			private int mLastFirstVisibleItem;

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				final int currentFirstVisibleItem = getListView()
						.getFirstVisiblePosition();

				if (currentFirstVisibleItem > mLastFirstVisibleItem) {
					((MainActivity) getActivity()).toggleActionBar(false);
				} else if (currentFirstVisibleItem < mLastFirstVisibleItem) {
					((MainActivity) getActivity()).toggleActionBar(true);
				}

				mLastFirstVisibleItem = currentFirstVisibleItem;
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {

			}
		});
	}

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
		if (!mLoaded) {
			mLoaded = true;
			EventBus.getDefault().post(new LoadDataRequest());
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		EventBus.getDefault().unregister(this);
	}

	@Override
	public void onDestroyView() {
		int topIndex = getListView().getFirstVisiblePosition();
		if (adapter != null) {
			Cursor cursor = (Cursor) adapter.getItem(topIndex);
			String Id = cursor.getString(cursor.getColumnIndex("Id"));
			SharedPreferencesUtil.getSharedPreferenecs(getActivity()).edit()
					.putString(KEY_LAST_BABY_ID, Id).commit();
		}
		super.onDestroyView();
	}

	public void onEventMainThread(LoadDataResult result) {
		Context context = getActivity();

		adapter = new BabyCursorAdapter(context, result.cursor, false);
		setListAdapter(adapter);

		String lastBabyID = SharedPreferencesUtil.getSharedPreferenecs(
				getActivity()).getString(KEY_LAST_BABY_ID, null);
		if (lastBabyID != null) {
			EventBus.getDefault().post(new SearchBabyRequest(lastBabyID));
		}
	}

	public static class BabyCursorAdapter extends CursorAdapter {

		private static final String TAG = "BabyCursorAdapter";

		public BabyCursorAdapter(Context context, Cursor c, boolean autoRequery) {
			super(context, c, autoRequery);
		}

		private static LayoutInflater sInflater;

		@Override
		public void bindView(View view, Context contex, Cursor cursor) {
			String photoWithFramePath = cursor.getString(cursor
					.getColumnIndex("PhotoWithFramePath"));
			String fullImagePath = String.format(
					"http://promo.nestle.com.hk/nanprobb/happytummy/%s",
					photoWithFramePath);
			String voteCount = cursor.getString(cursor
					.getColumnIndex("VoteCount"));
			String rank = cursor.getString(cursor.getColumnIndex("Rank"));

			Log.d(TAG, String.format("fullImagePath[%s]", fullImagePath));
			ImageView imageView = (ImageView) view.findViewById(R.id.image);
			Picasso.with(contex).load(fullImagePath).fit().into(imageView);

			TextView voteCountText = (TextView) view
					.findViewById(R.id.vote_count);
			voteCountText.setText(voteCount + " 票");
			TextUtil.applyCustomFont(contex, voteCountText);

			TextView rankText = (TextView) view.findViewById(R.id.rank);
			rankText.setText("#" + rank);
			TextUtil.applyCustomFont(contex, rankText);
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup container) {
			if (sInflater == null) {
				sInflater = (LayoutInflater) context
						.getSystemService(Service.LAYOUT_INFLATER_SERVICE);
			}
			View rootView = sInflater.inflate(R.layout.baby_item, container,
					false);
			return rootView;
		}

	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Cursor cursor = (Cursor) adapter.getItem(position
				- getListView().getHeaderViewsCount());
		String Id = cursor.getString(cursor.getColumnIndex("Id"));
		openVoteUrlInBrowser(Id);
	}

	private void openVoteUrlInBrowser(String Id) {
		String voteUrl = String
				.format("https://promo.nestle.com.hk/nanprobb/happytummy/vote.aspx?id=%s",
						Id);
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(voteUrl));
		startActivity(i);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_rank_list, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemID = item.getItemId();
		if (itemID == R.id.action_search) {
			showDialogBabyID();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void showDialogBabyID() {
		BabyIDDialogFragment f = (BabyIDDialogFragment) getChildFragmentManager()
				.findFragmentByTag("search_baby_id");
		if (f == null) {
			f = new BabyIDDialogFragment();
			f.show(getChildFragmentManager(), "search_baby_id");
		}
	}

	public void onEventMainThread(SearchBabyResult result) {
		if (result.index == -1) {
			Toast.makeText(getActivity(), "找不到", Toast.LENGTH_SHORT).show();
		} else {
			getListView().setSelection(result.index);
		}
	}
}
