package org.dyndns.warenix.nanprobb_rank;

import org.dyndns.warenix.nanprobb_rank.MainActivity.TextUtil;
import org.dyndns.warenix.nanprobb_rank.eventbus.SearchBabyRequest;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import de.greenrobot.event.EventBus;

public class BabyIDDialogFragment extends DialogFragment implements
		OnEditorActionListener {
	private EditText mEditText;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_search_baby_id,
				container);
		Context context = getActivity();
		mEditText = (EditText) view.findViewById(R.id.baby_id);
		TextUtil.applyCustomFont(getActivity(), mEditText);
		getDialog().setTitle("輸入 BB ID. e.g. 100xxxx");

		// Show soft keyboard automatically
		mEditText.requestFocus();
		getDialog().getWindow().setSoftInputMode(
				LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		mEditText.setOnEditorActionListener(this);

		return view;
	}

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if (EditorInfo.IME_ACTION_DONE == actionId) {
			String Id = mEditText.getText().toString();
			EventBus.getDefault().post(new SearchBabyRequest(Id));
			this.dismiss();
			return true;
		}
		return false;
	}

}
