package org.dyndns.warenix.nanprobb_rank.eventbus;

import android.database.Cursor;

/**
 * A cursor to the baby ranking
 * 
 * @author warenix
 * 
 */
public class LoadDataResult {
	public Cursor cursor;

	public LoadDataResult(Cursor cursor) {
		this.cursor = cursor;
	}
}
